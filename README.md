## How to start

In the project directory, you can run:
### `yarn` or `npm install`

Then run 
### `yarn start` or `npm start`

## Technology Used:
| Name          | Version       |
| ------------- |:-------------:|
| React         | "^16.6.3"     |
| react-redux   | "^5.1.1"      |
| redux-thunk   | "^2.3.0"      |
| Lodash        | "^4.17.11"    |

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Screenshot

![alt text](https://bitbucket.org/rbrahul/react-articles/raw/9cfa606ea76f32f256ea83d838ac2e4d184f7659/screen-shot.png "Screenshot")

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
