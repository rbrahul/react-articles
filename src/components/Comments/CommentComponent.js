import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CommentItem from './../CommentItem';
import  './../../images/comment.svg'
import './Comments.css';


class Comments extends Component {
    state = {
        visibility: false
    };

    toggleComment() {
        this.setState(currentState => ({ visibility: !currentState.visibility }));
    }

    renderComments() {
        return (<div className="comment-list">
            {
                this.props.comments.map((comment, index) => {
                    return <CommentItem key={index} comment={comment} />
                })
            }
        </div>);
    }

    render() {
        return (<div className="comment-area">
            <div className="comment-counter">
                <div onClick={this.toggleComment.bind(this)}>
                    {!this.state.visibility && <span>Show </span>}
                    {this.state.visibility && <span>Hide </span>}
                    {this.props.comments.length} Comments</div>
            </div>
            {this.state.visibility && this.renderComments()}
        </div>);
    }
};

Comments.propTypes = {
    comments: PropTypes.array.isRequired
};

export default Comments;