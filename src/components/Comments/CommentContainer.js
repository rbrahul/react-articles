import { connect } from 'react-redux';
import Comment from './CommentComponent';

const mapStateToProps = (state, ownProps) => {
    const comments = state.comments.items[ownProps.articleId] || [];
    return {
        comments,
    };
};

export default connect(mapStateToProps)(Comment);
