import React from 'react';
import PropTypes from 'prop-types';
import Comments from './../Comments'
import './Article.css';

const Article = props => {
    return (<div className="article">
        <div className="content-area">
            <h1 className="title">{props.article.title}</h1>
            <p>{props.article.body}</p>
        </div>
        <Comments articleId={props.article.id} />
    </div>);
};

Article.propTypes = {
    article: PropTypes.shape({
        id: PropTypes.number.isRequired,
        userId: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        body: PropTypes.string.isRequired
    })
}
export default Article;