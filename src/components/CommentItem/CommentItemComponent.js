import React from 'react';
import PropTypes from 'prop-types';
import './CommentItem.css';
import  './../../images/comment.svg'

const CommentItem = props => {
    return (<div className="comment-item">
        <p className="username">{props.comment.name}</p>
        <p className="comment">{props.comment.body}</p>
    </div>);
};


CommentItem.propTypes = {
    comment: PropTypes.shape({
        name: PropTypes.string.isRequired,
        body: PropTypes.string.isRequired,
    })
};

export default CommentItem;
