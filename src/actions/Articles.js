import * as ACTION_TYPES from './../constants/ActionTypes';

const updateArticles = articles => {
    return {
        type: ACTION_TYPES.UPDATE_ARTICLES,
        data: articles
    }
}

export const fetchArticles = () => async dispatch => {
    try {
        const response = await fetch('https://jsonplaceholder.typicode.com/posts/', {
            method: 'GET',
        });
        const articles = await response.json();
        return dispatch(updateArticles(articles));
    } catch (error) {
        console.error('ARTICLE API CALLING FAILED', error);
    }
};
