import groupBy from 'lodash/groupBy';
import * as ACTION_TYPES from './../constants/ActionTypes';

const updateComments = comments => {
    const groupedComments = groupBy(comments, item => item.postId);
    return {
        type: ACTION_TYPES.UPDATE_COMMENTS,
        data: groupedComments
    }
}

export const fetchComments = () => async dispatch => {
    try {
        const response = await fetch('https://jsonplaceholder.typicode.com/comments/', {
            method: 'GET',
        });
        const comments = await response.json();
        return dispatch(updateComments(comments));
    } catch (error) {
        console.error('COMMENTS API CALLING FAILED', error)
    }
};
