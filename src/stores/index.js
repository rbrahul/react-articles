import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import AppReducer from '../reducers/AppReducer';

const middleware = [thunk];
let extension = next => next;
extension = window.devToolsExtension ? window.devToolsExtension() :extension;
 
const store = createStore(
    AppReducer,
    compose(applyMiddleware(...middleware), extension)
);

export default store;
