import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchArticles } from './../actions/Articles'
import { fetchComments } from './../actions/Comments'
import App from './App.js';

const mapStateToProps = state => {
    return {
        articles: state.articles.items
    };
};


const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            fetchArticles,
            fetchComments
        },
        dispatch
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
