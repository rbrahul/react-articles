import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Article from '../components/articles';
import './App.css';

class App extends Component {
  componentDidMount() {
    this.props.fetchArticles();
    this.props.fetchComments();
  }

  render() {
    return (
      <div className="wrapper">
        <div className="header">
          <h1 className="logo">Articles</h1>
        </div>
        <div className="container">
          <div className="article-list">
            {
              this.props.articles.map((item, index) => <Article key={index} article={item} />)
            }
          </div>
        </div>
      </div>
    );
  }
}

App.propTypes = {
  fetchArticles: PropTypes.func.isRequired,
  fetchComments: PropTypes.func.isRequired
};

export default App;
