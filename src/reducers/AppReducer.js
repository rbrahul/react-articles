import { combineReducers } from 'redux';
import articles from './Articles';
import comments from './Comments';

const AppReducer = combineReducers({
    articles,
    comments
});

export default AppReducer;