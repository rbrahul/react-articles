import * as ACTION_TYPES from './../constants/ActionTypes';

const initialState = {
    items: {}
};

export default function(state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.UPDATE_COMMENTS:
            return {
                items: action.data
            };
        default:
            return state;
    }
}
