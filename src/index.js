import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './app';
import store from './stores';
import './index.css';
import * as serviceWorker from './serviceWorker';

class RootApp extends Component {
    render() {
        return (<Provider store={store}>
            <App />
        </Provider>);
    }
}


ReactDOM.render(<RootApp />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
